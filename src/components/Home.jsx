import Shooter from "./Shooter";
import CategoriesCart from "./Categories-cart";
import Site from "./Site";
import Brend from "./Brend";
import OurAdvantages from "./OurAdvantages";
import Comanda from "./Comanda";
import OnlineStoreTeplodom from "./OnlineStoreTeplodom";
import Karusel from "./Karusel";

function Home() {
  return (
    <>
      <Karusel />
      <Shooter path={"categories"} name={"Категории"} />
      <CategoriesCart />
      <Shooter path={"itemsSite"} name={"Новинки на сайте"} />
      <Site />
      <Brend />
      <Shooter path={"itemsSite"} name={"Популярные товары"} />
      <Site />
      <OurAdvantages/>
      <Comanda/>
      <OnlineStoreTeplodom/>
    </>
  );
}

export default Home;
