import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { l2, l3 } from "../assets";
import { deleteBasket } from "../reducers/basketReducer";
import PlacingOrder from "../features/Oform/Oform";
import { message } from "antd";
import { NavLink } from "react-router-dom";

function Basket() {
  const { basket } = useSelector((state) => state.basket);
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [id, setId] = useState("");
  const dispatch = useDispatch();
  const deleteItem = (id) => {
    dispatch(deleteBasket(id));
  };
  const handleOrder = (text) => {
    setOpen(true);
    setTitle(text.brand);
    setId(text.id);
    console.log(text);
  };
  const handleDelete = (id) => {
    deleteItem(id);
    message.open({
      type: "error",
      content: "удалено из корзину",
    });
  };
  return (
    <>
      <div className="lg:container mx-auto px-4 mb-20">
        <h2 className=" text-[20px] font-bold mb-[30px] sm:text-[30px]">
          Корзинка
        </h2>
        <div className="grid grid-cols-1 xs:grid-cols-2 sm:grid-cols-4 gap-4">
          {basket &&
            basket
              .filter((item) => item.id == item.id)
              .map((product) => {
                return (
                  <div
                    className="bg-white rounded-2xl w-[255px] h-[401px] relative"
                    key={product.id}
                  >
                    <NavLink to={`/products/${product.id}`} className="">
                      <img
                        className="mx-auto my-4 bg-cover w-[177px] h-[177px bg-cover"
                        src={product.image}
                        alt=""
                      />
                    </NavLink>
                    <h1 className="text-sm w-[240px] px-4 absolute top-60">
                      {product.name}
                    </h1>
                    <p className="px-4 capitalize font-semibold text-1xl line-clamp-1 mb-1 ">
                      {product.brand}
                    </p>
                    <p className="px-4 capitalize font-medium text-xl  mb-[22px] line-clamp-1">
                      {product.category}
                    </p>
                    <h2 className="px-4  pt-5 font-bold">
                      {Math.floor(product.price / 1000)}
                      <span> 000</span> sum
                    </h2>
                    <div className="flex px-4 mt-3 gap-2 text-white ">
                      <button
                        onClick={() => handleOrder(product)}
                        className="bg-yellow-500 hover:bg-yellow-400 py-2 px-[40px] flex items-center text-white rounded-lg cursor-pointer  border-2 "
                      >
                        <img src={l3} alt="" className="bg-cover" />

                        <span className="ml-1">Оформить</span>
                      </button>

                      <button
                        onClick={() => handleDelete(product.id)}
                        className="text-white rounded-lg cursor-pointer bg-yellow-500 py-2 px-[6px] hover:bg-red-500 border-2 hover:border-2  hover:border-red-500 hover:text-red-500 "
                      >
                        <img src={l2} alt="" className="bg-cover" />
                      </button>
                    </div>
                  </div>
                );
              })}
        </div>
      </div>
      {open && <PlacingOrder title={title} id={id} />}
    </>
  );
}

export default Basket;
