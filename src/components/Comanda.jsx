import Slider from "./slider/Slider";

const comanda = [
  {
    id: 1,
    img: "/photos/person.jpg",
    author: "Jasur Rahmatov",
    title: "Директор фирмы",
  },
  {
    id: 2,
    img: "/photos/person2.jpg",
    author: "Farrux Rahmatov",
    title: "Менеджерпо продажам",
  },
  {
    id: 3,
    img: "/photos/person3.jpg",
    author: "Omadbek Abdullayev",
    title: "Менеджерпо продажам",
  },
  {
    id: 4,
    img: "/photos/person4.jpg",
    author: "Elmurod Qarshiev",
    title: "Менеджерпо продажам",
  },
];
function Comanda() {
  return (
    <div className=" lg:container mx-auto px-4">
      <Slider>Наши команда</Slider>
      <div className=" mb-14 mt-10 overflow-x-scroll lg:overflow-hidden flex justify-between items-center gap-6 box-border">
        {comanda.map((item) => {
          return (
            <div key={item.id} className=" bg-white py-[30px] px-[30px] rounded-[15px] w-[257px]">
              <img src={item.img} alt={item.author} className=" cursor-pointer bg-cover mb-[30px] mx-auto w-[165px] h-[165px] rounded-full" />
              <h3 className=" w-44 mx-auto text-center text-[18px] font-medium  mb-[10px]">{item.author}</h3>
              <p className=" w-44 mx-auto text-sm text-stone-500 text-center font-medium">{item.title}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Comanda;
