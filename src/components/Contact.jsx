import  { useState } from "react";
import { c11, c2, c3, c4, c5,  } from "../assets";
import Info from "./Info";
import { message } from 'antd';

const contact = [
  {
    id: 1,
    img: c11,
    title: "Телефон",
    text: "+998 (94) 976 09 26",
  },
  {
    id: 2,
    img: c2,
    title: "Локация",
    text: " ул. Sergeli, рынок moshina bozor, дом 110, магазин 44",
  },
  {
    id: 3,
    img: c3,
    title: "Е-майл",
    text: " rakhmatovjasur3@gmail.com",
  },
  {
    id: 4,
    img: c11,
    title: "Телефон",
    text: " +998 (93) 228 09 06",
  },
  {
    id: 5,
    img: c4,
    title: "Расписание",
    text: " Мы в вашем распоряжении 7 дней в неделю!",
  },
  {
    id: 6,
    img: c5,
    title: "Время",
    text: "Каждый день с 8:00 – 18:00",
  },
];

function Contact() {
  const [phone, setPhone] = useState("");
  const [commet, setCommet] = useState("");
  const [name, setName] = useState("");
  let token = "6263303825:AAGC2ufGs5slHMCcsHnJJCtx4oWDJIwfSRQ";
  let chat_id = "2001241145";
  const remove = () => {
    setCommet("");
    setName("");
    setPhone("");
  };
  const user = `
  Name: ${name};
  Phone: ${phone};
  Description: ${commet}
  `;
  let url = `https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${user}`;
  const sendMessage = (e) => {
    e.preventDefault();
    message.open({
      type: "success",
      content: "Sent your telegam bot",
    });
    const api = new XMLHttpRequest();
    api.open("POST", url, true);
    api.send();
    remove();
  };

  return (
    <div className=" lg:container mx-auto px-4">
      <Info>Контакты</Info>

      <div className=" mt-5 sm:mt-10 flex justify-center items-center gap-5 sm:gap-10 text-center  flex-wrap mb-[80px]">
        {contact.map((item) => (
          <div key={item.id} className=" flex flex-col  justify-center  box-border items-center w-[100%] xs:w-[40%] sm:w-[30%] h-[215px]">
            <div className=" w-[120px] h-[120px] box-border g  border-[5px] rounded-full flex-wrap  border-borderColor   flex items-center justify-center">
              <img src={item.img} alt={item.title} className=" w-[57px] h-[57px] bg-cover" />
            </div>
            <h3 className=" mt-1 font-bold tracking-widest text-xl">{item.title}</h3>
            <p className=" mt-1 font-semibold text-[18px] line-clamp-2  sm:line-clamp-1 ">{item.text}</p>
          </div>
        ))}
      </div>
      <Info>Заказать обратный звонок</Info>

      <div className=" mt-6 flex items-center justify-between flex-col md:flex-row mb-20 gap-10 ">
        <form id="form" className=" flex flex-col gap-5 w-full sm:flex-1">
          <label className=" flex flex-col gap-2">
            <span>Введите имя</span>
            <input required id="text" type="text" className="w-full pl-2  outline-none border-[2px] rounded-md border-gray-400 py-5" value={name} onChange={(e) => setName(e.target.value)} />
          </label>
          <label className=" flex flex-col gap-2">
            <span>Введите номер телефона</span>
            <input required type="text" className="w-full pl-2  outline-none border-[2px] rounded-md border-gray-400 py-5" value={phone} onChange={(e) => setPhone(e.target.value)} />
          </label>
          <label className=" flex flex-col gap-2">
            <span>Комментарии</span>
            <textarea required className="w-full pl-2  outline-none border-[2px] rounded-md border-gray-400 h-[100px] resize-none" value={commet} onChange={(e) => setCommet(e.target.value)}></textarea>
          </label>
          <button className=" bg-btnsBgColor text-white py-2 w-[106px] rounded-md outline-none hover:opacity-90" onClick={sendMessage}>
            Отправить
          </button>
        </form>
        <div className=" flex-1">
          <iframe className=" rounded-[20px] min-w-[350px] sm:min-w-[700px]  md:min-w-full h-[400px]" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d390662.4402681316!2d66.53297773679874!3d40.09659112892341!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f4d63841abe9ecd%3A0xf19a87f901d74451!2z0J_QsNC50LDRgNGL0LrRgdC60LjQuSDRgNCw0LnQvtC9LCDQodCw0LzQsNGA0LrQsNC90LTRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwg0KPQt9Cx0LXQutC40YHRgtCw0L0!5e0!3m2!1sru!2s!4v1711886664994!5m2!1sru!2s" frameBorder="1" allowFullScreen={true} style={{ position: "relative" }}></iframe>
        </div>
      </div>
    </div>
  );
}

export default Contact;
