import { useDispatch, useSelector } from "react-redux";
import { deleteLike, setBasket } from "../reducers/basketReducer";
import { message } from "antd";
import { l1, l2 } from "../assets";
import { NavLink } from "react-router-dom";

function Liked() {
  const dispatch = useDispatch();
  const { like } = useSelector((state) => state.basket);
  const handlerBasket = (item) => {
    dispatch(setBasket(item));
    message.open({
      type: "success",
      content: "добавлено в корзину",
    });
  };
  const deleteItem = (id) => {
    dispatch(deleteLike(id));
    message.open({
      type: "error",
      content: "удалено из избранные",
    });
  };
  return (
    <div className="lg:container mx-auto px-4 mb-20">
      <h2 className=" text-[20px] font-bold mb-[30px] sm:text-[30px]">Избранные товары</h2>
      <div className="grid grid-cols-2 sm:grid-cols-4 sm:gap-3">
        {like &&
        like.map((product) => {
          return (
            <div
              className="bg-white rounded-2xl w-[255px] h-[401px] relative"
              key={product.id}
            >
              <NavLink to={`/products/${product.id}`} className="">
                <img
                  className="mx-auto my-4 bg-cover w-[177px] h-[177px]"
                  src={product.image}
                  alt=""
                />
              </NavLink>
              <h1 className="text-sm w-[240px] px-4 absolute top-60">
                {product.name}
              </h1>
              <p className="px-4 capitalize font-semibold text-1xl line-clamp-1 mb-1 ">
                {product.brand}
              </p>
              <p className="px-4 capitalize font-medium text-xl  mb-[22px] line-clamp-1">
                {product.category}
              </p>
              <h2 className="px-4  pt-5 font-bold">
                {Math.floor(product.price / 1000)}
                <span> 000</span> sum
              </h2>
              <div className="flex px-4 mt-3 gap-2 text-white ">
                <button
                  onClick={() => handlerBasket(product)}
                  className="bg-yellow-500 hover:bg-yellow-400 py-2 px-[40px] flex items-center text-white rounded-lg cursor-pointer  border-2 "
                >
                  <img src={l1} alt="" />

                  <span className="ml-2">B_корзину</span>
                </button>

                <button
                  onClick={() => deleteItem(product.id)}
                  className="text-white rounded-lg cursor-pointer bg-yellow-500 py-2 px-[6px] hover:bg-red-500 border-2 hover:border-2  hover:border-red-500 hover:text-red-500 "
                >
                  <img src={l2} alt="" />
                </button>
              </div>
            </div>
          );
        })} 
      </div>
    </div>
  );
}

export default Liked;

